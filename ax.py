
import os
from subprocess import run, PIPE

def run_command_and_return(command):
    return run(command, shell=True, stdout=PIPE).stdout.decode('utf-8').strip()
def partition():
    os.system("clear")
    def run_cfdisk(disk_name):
        os.system(f"sudo cfdisk /dev/{disk_name}")
    def physical_drives():
        blacklist = ["loop"]
        output_string = run_command_and_return('lsblk -d -o name -n')
        return [x for x in output_string.split('\n') if x not in blacklist]
    user_quit = False
    all_disk = physical_drives()
    print("****Welcome to AX-partitioning utility****\n")
    while(not(user_quit)):
        print("enter the drive name you want to partition\nenter 'q' to quit")
        os.system("lsblk -d -o NAME,SIZE")
        user_input = input(">").replace("/dev/", "").replace("dev/", "")
        if (user_input == "q"):
            user_quit = True
            os.system("clear")
            input("**looks like the partioning is complete, now the script will move on to formatting the disk**\npress any key to continue\n")
            break
        if (user_input in all_disk):
            run_cfdisk(user_input)
        else:
            print("disk name or command entered is not recognised, please try again")
            input("press any key to continue\n")
        os.system("clear")
def formatting():
    user_quit = False
    print("***welcome to AX-farmating utility***")
    while (not(user_quit)):
        os.system("lsblk -o NAME,SIZE")

        
#script starts here
partition()
os.system("./scripts/0-preinstall.sh")
os.system("./scripts/1-setup.sh")
os.system("./scripts/2-user.sh")
os.system("./scripts/3-post-setup.sh")
os.system("./scripts/kderice-backup.sh")
os.system("./scripts/startup.sh")